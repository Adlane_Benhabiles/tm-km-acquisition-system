from owlready2 import *
onto  = get_ontology("http://test.org/t.owl")

with onto :

        #ontologie du domaine
        class Network(Thing):
            pass
        class TraficEntity(Network):
            pass
        
        class Datagram(TraficEntity):
            pass
        
        class Flow(TraficEntity):
            pass

        #

        class Event(Network):
            pass
        
        class Trafic_Evt(Event):
            pass
        ##
        class LinkUsageEvt(Trafic_Evt):
            pass
        class QueueUsage_Evt(Trafic_Evt):
            pass
        class QueueOverflow_Evt(QueueUsage_Evt):
            pass

        
        class LostPkt_Evt(Event):
            pass
        ##
        class  TTL_LostPkt(LostPkt_Evt):
            pass
        class RT_LostPkt(LostPkt_Evt):
            pass

        
        class StateChange_Evt(Event):
            pass
        ##
        class NodeStateChange(StateChange_Evt):
            pass
        class LinkStateChange(StateChange_Evt):
            pass
        class RTChange(StateChange_Evt):
            pass
        class IFaceStateChange(StateChange_Evt):
            pass
        
        # 
        class Database(Network):
            pass
        ##
        class LDB(Database):
            pass
        class GDB(Database):
            pass


        #
        class ManagementTool(Network):
            pass

        class Capsule(ManagementTool):
            pass
        
        class Sensor(ManagementTool):
            pass
        ##
        class NeighStatus_Sns(Sensor):
            pass
        class RoutingTable_Sns(Sensor):
            pass
        class RT_LostPkt_Sns(Sensor):
            pass
        class TTL_LostPktSns(Sensor):
            pass
        class QueueUsage_Sns(Sensor):
            pass
        class LinkUsage_Sns(Sensor):
            pass

        #
        class TraficStatistics(Network):
            pass
        ##
        class TMEntry(TraficStatistics):
            pass
        class TraficMatrix(TraficStatistics):
            pass
        
        #
        class Action(Network):
            pass
        ##
        class RestoreAction(Action):
            pass
        ###
        class RestoreIFace_Actn(RestoreAction):
            pass
        class RestoreLink_Actn(RestoreAction):
            pass
        class RestoreNode_Actn(RestoreAction):
            pass
        class CommandAction(Action):
            pass
        ####
        class SwitchLink_Cmd(CommandAction):
            pass
        class ChangeLinkCoast_Cmd(CommandAction):
            pass
        class TuneSensor_Cmd(CommandAction):
            pass
        class SwitchRecording_Cmd(TuneSensor_Cmd):
            pass
        class ChangeSamlingRate_Cmd(TuneSensor_Cmd):
            pass
        class ChangeRecordingRate_Cmd(TuneSensor_Cmd):
            pass
        class ChangethresholdRate_Cmd(TuneSensor_Cmd):
            pass
        class ChangeSwitchNotification_Cmd(TuneSensor_Cmd):
            pass
        class DBAction(Action):
            pass
        class BasicAction(Action):
            pass
        class NotifyAction(Action):
            pass

        #
        class Demand(Network):
            pass
        ##
        class DMEntry(Demand):
            pass
        class DemandMatrix(Demand):
            pass




        #
        class Actor(Network):
            pass
        
        class PLA(Actor):
            pass

        class Reasoner(Actor):
            pass
        ##
        class OffLR(Reasoner):
            pass
        class OnLR(Reasoner):
            pass


        #
        class Routing(Network):
            pass

        #
        class Abnomality(Network):
            pass

        class RT_Corruption(Abnomality):
            pass

        class Congestion(Abnomality):
            pass
        
        class NetDisconnection(Abnomality):
            pass
        
        class Loop(Abnomality):
            pass
        
        #
        class NetEntity(Network):
            pass

        class SWNetEntity(NetEntity):
            pass

        class HWNetEntity(NetEntity):
            pass


        ##
        #
        class Node(HWNetEntity):
            pass
        
        class Host(Node):
            pass

        class Router(Node):
            pass

        ##
        class Link(HWNetEntity):
            pass

        ##
        class IFace(HWNetEntity):
            pass



        ##
        
        class Queue(SWNetEntity):
            pass

        class RoutingTable(SWNetEntity):
            pass
        
        class RTEntry(SWNetEntity):
            pass






        #ontologie de l'application

        class Document(Thing):
            pass
        
        class resume(DataProperty): 
            domain = [Document]
            range = [str]
        
        class nuage_de_mots(DataProperty):
            domain = [Document]
            range = [str]

        class Title(DataProperty): 
            domain = [Document]
            range = [str]

        class Creator(DataProperty): 
            domain = [Document]
            range = [str]

        class Subject(DataProperty): 
            domain = [Document]
            range = [str]

        class Description(DataProperty): 
            domain = [Document]
            range = [str]

        class Publisher(DataProperty): 
            domain = [Document]
            range = [str]

        class Contributor(DataProperty): 
            domain = [Document]
            range = [str]
        
        class Date(DataProperty): 
            domain = [Document]
            range = [str]

        class Type(DataProperty): 
            domain = [Document]
            range = [str]

        class Format(DataProperty): 
            domain = [Document]
            range = [str]
        
        class Identifier(DataProperty): 
            domain = [Document]
            range = [str]

        class Source(DataProperty): 
            domain = [Document]
            range = [str]

        class Language(DataProperty): 
            domain = [Document]
            range = [str]

        class Relation(DataProperty): 
            domain = [Document]
            range = [str]
        
        class Coverage(DataProperty): 
            domain = [Document]
            range = [str]
        
        class Rights(DataProperty): 
            domain = [Document]
            range = [str]
        

        



        
        
        class Term(Thing):
            pass
        
        class Frequency(Thing):
            pass
        


        class Cluster(Thing):
            pass
        class Clustering(Thing):
            pass
        
        class date(DataProperty):
            domain = [Frequency, Clustering]
            range = [str]

        
        class is_in_cluster(ObjectProperty):
            domain = [Document]
            range = [Cluster]

        class is_in_clustering(ObjectProperty):
            domain = [Cluster]
            range = [Clustering]


        class has_frequency_of(ObjectProperty):
            domain = [Document]
            range = [Frequency]

        class his_frequency_is(ObjectProperty):
                domain = [Term]
                range = [Frequency]

        class frequency_is(DataProperty):
            domain = [Frequency]
            range = [int]
#onto.save(file = "t.owl", format = "rdfxml")
#print(Network.descendants())