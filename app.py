

import streamlit as st 
import datetime
import secrets
import os
import re
import hashlib
from owlready2 import *
from onto_class import *
from pathlib import Path


FREQ_TERM_DIR = "freqsTerm"
NUAGES_MOTS_DIR = "nuagesMots"
RESUMES_DIR = "resumes"
CLUSTER_DIR = "cluster"
CLASSIF_DIR =  "classif"

lang = "Anglais"

onto = None

if not os.path.exists(FREQ_TERM_DIR):
    os.makedirs(FREQ_TERM_DIR)

if not os.path.exists(NUAGES_MOTS_DIR):
    os.makedirs(NUAGES_MOTS_DIR)


if not os.path.exists(RESUMES_DIR):
    os.makedirs(RESUMES_DIR)

if not os.path.exists(CLUSTER_DIR):
    os.makedirs(CLUSTER_DIR)

if not os.path.exists(CLASSIF_DIR):
    os.makedirs(CLASSIF_DIR)



st.set_page_config(page_title='TM/KM Aquisition system',
                   page_icon = "icon.png",
                   
                   initial_sidebar_state = 'auto')
                    #layout = 'wide',
footer = """ 
            <style>footer {
    
                visibility: hidden;
    
            }

"""
#footer

#            footer:after {
#                content:'Université Saad Dahlab Blida1'; 
#                visibility: visible;
#                display: block;
#                position: relative;
#                #background-color: red;
#                padding: 5px;
#                top: 2px;
#                <style>
#            }



st.markdown(footer, unsafe_allow_html=True)

@st.cache()
def hacher(mdp):
    return hashlib.sha256(str.encode(mdp)).hexdigest()
@st.cache()
def verif_conx(password, username):
    mdp_hashe = hacher(password)
    results = session.query(Utilisateur).all()
    print(password)
    print(hacher(password))
    for r in results:

        if (r.user_name == username) and (mdp_hashe == r.mdp):
            print(r.mdp)
            return True
    return False
def trouve_extension(filename):

    import os
    split_tup = os.path.splitext(filename)
    return split_tup[1]


def retouner_string(file):

    if trouve_extension(file.name)==".txt":
        
        from io import StringIO
        stringio = StringIO(file.getvalue().decode("utf-8"))
        string_data = stringio.read()
        return string_data

    elif trouve_extension(file.name)==".pdf" :
        
        import pdftotext
        pdf = pdftotext.PDF(file)
        alltxt = "\n\n".join(pdf)
        return alltxt
        
    elif trouve_extension(file.name)==".docx" or trouve_extension(file.name)==".doc":
        import docx2txt
        text = docx2txt.process(file)
        return text


@st.cache()
def ng_mots(string, fname):
    
    from wordcloud import WordCloud, STOPWORDS
    from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
    stopwords = set.union(STOPWORDS, fr_stop)
    WC = WordCloud(
        background_color="white",
        stopwords=stopwords,
        height = 400, 
        width = 600
    )
    
    WC.generate(string)
    fname = re.sub('[\+]', ' ', fname)
    WC.to_file(NUAGES_MOTS_DIR+"/"+fname+".png")



st.cache()
def freq_term(string):
    from spacy.lang.fr.stop_words import STOP_WORDS as fr_stop
    import spacy
    from collections import Counter
    global lang
    if lang == "Français":
        nlp = spacy.load("fr_core_news_sm")

    elif lang == "Anglais" :
        nlp = spacy.load('en_core_web_sm')
     

    a_list = string.split()

    new_string = " ".join(a_list)

    doc = nlp(new_string)

    words = [token.text
            for token in doc
        if not token.is_stop and not token.is_punct]
    words = [word for word in words if not word.isdigit()] 
    symbols = {'~', ':', "'", '+', '[', '\\', '@', '^', '{', '%', '(', '-', '"', '*', '|', ',', '&', '<', '`', '}', '.', '_', '=', ']', '!', '>', ';', '?', '#', '$', ')', '/'}
    words_nonsymboles=[]
    for word in words :
        if word not in symbols and word not in fr_stop:
            word = word.lower()
            words_nonsymboles.append(word)
    # words_nonsymboles = filter (lambda wrd : len (wrd) > 5, words_nonsymboles)
    word_freq = Counter(words_nonsymboles)

    common_words = word_freq.most_common(10)

    import pandas as pd 
    df = pd.DataFrame(
    common_words,
    columns=("Terme","Frequence")
    )
    return df

@st.cache()
def summer_text(string):
    from sumy.parsers.plaintext import PlaintextParser
    from sumy.nlp.tokenizers import Tokenizer
    from sumy.summarizers.lsa import LsaSummarizer as Summarizer
    from sumy.nlp.stemmers import Stemmer
    from sumy.utils import get_stop_words
    import textwrap

    global lang
    if lang == "Anglais"  :
        LANGUAGE = "english"

    elif lang == "Français":
        LANGUAGE = "french"

    



    # SENTENCES_COUNT = 10


    SENTENCES_COUNT = int(0.0008*len(string))

    parser = PlaintextParser.from_string(string, Tokenizer(LANGUAGE))
    stemmer = Stemmer(LANGUAGE)

    summarizer = Summarizer(stemmer)
    summarizer.stop_words = get_stop_words(LANGUAGE)
 
    ss=""
    for s in summarizer(parser.document, SENTENCES_COUNT):
        
        s = ''.join((item for item in str(s))) #if not item.isdigit()))
        ss=ss+" "+s

    ss = textwrap.fill(ss)
    return ss


def ouvrir_dossier(DOSSNAME):
    

    import platform
    
    if platform.system() == "Linux":
        os.system("thunar " + DOSSNAME)
    elif platform.system() == "Windows":
        os.system("start " + DOSSNAME)    



terms = []
def term_freq_ui(fdatas_list):

    st.markdown("**Frequence des termes :**")


    
    for fdata in fdatas_list:
        import pandas as pd 
        df = freq_term(fdata[0])
        
        with st.expander("Frequence des termes : "+ fdata[1]):
            st.dataframe(df)
        onto = get_ontology("t.owl").load()   
        with onto:
            doc = Document(fdata[1])
            terms=[]
            for i in df.index: 
                
                term = Term(str(df["Terme"][i]))
                #print("\n")
                #print(term)
                #print("\n")

                
                do = pd.read_csv('terms.csv')
                #print(do["term"].tolist())
                #print("\n")
                ttm = Term.instances()
                ttm = [str(i) for i in ttm]
                #print(ttm)
                #print("\n")
                lat =list(do["term"])
                #print(type(lat))
                lat.append(ttm)
                
                if not(str(term) in lat) :
                    print(str(term))
                    terms.append(term)
                    print(terms)

                
                
                frequency = Frequency(str(secrets.token_hex(nbytes=8)))
                today = datetime.date.today()
                frequency.date.append(str(today))
                doc.has_frequency_of.append(frequency)
                term.his_frequency_is.append(frequency)
                frequency.frequency_is.append(int(df["Frequence"][i]))
                
            #print(Term.instances())
            dterm  = {"term":terms}
            
            d = pd.DataFrame(dterm)
            #print(dterm)
            dv = pd.read_csv("terms.csv")
            dv = pd.concat([dv, d], ignore_index=True)
            dv.to_csv("terms.csv", index= False)

        fname = re.sub('[\+]', ' ', fdata[1])
        df.to_csv(FREQ_TERM_DIR+'/'+fname+".csv", index= False)
        onto.save(file = "t.owl", format = "rdfxml")


def nuage_mots_ui(fdatas_list) :

    st.markdown("**Nuage de mots :**")
    from PIL import Image
    onto = get_ontology('t.owl').load()
    for fdata in fdatas_list:


        ng_mots(fdata[0], fdata[1] )


        fname = re.sub('[\+]', ' ', fdata[1])
        path_ngm = NUAGES_MOTS_DIR+"/"+fname+".png"
        
        with onto:
            doc = Document(str(fdata[1]))
            doc.nuage_de_mots.append(path_ngm)
            
        image = Image.open(path_ngm)
        with st.expander("Nuage de mot de : "+ fdata[1]):
                                    
            st.image(image, caption=fdata[1])
    onto.save(file = "t.owl", format = "rdfxml")

def save_resume(string, fname):
    import re

    fname = re.sub('[\+]', ' ', fname)
    text_file = open(RESUMES_DIR+"/"+"resume_"+fname+".txt", "w")
 
    text_file.write(string)
 
    text_file.close()



def resume_ui(fdatas_list):

    st.markdown("**Resumé :**")

 
        
    for fdata in fdatas_list:


        sm_txt = summer_text(fdata[0])
        doc = Document(str(fdata[1]))
        path_resume = RESUMES_DIR+"/"+"resume_"+fdata[1]+".txt"
        doc.resume.append(path_resume)
        with st.expander("Resumé : "+ fdata[1]):
                                        
            st.text(sm_txt)
        save_resume(sm_txt, fdata[1])

  

from sqlalchemy import create_engine
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy import inspect
base = declarative_base()


class Utilisateur(base):
            
        __tablename__ = 'utilisateur'
        utilisateur_id = Column(Integer, primary_key=True,autoincrement=True)
        nom = Column(String, nullable=False)
        prenom = Column(String, nullable=False)
        user_name = Column(String, nullable=False, unique=True)
        email = Column(String, nullable=False, unique=True)
        mdp = Column(String, nullable=False)
        date_naissance = Column(String, nullable=False)
        nom_service = Column(String, nullable=False)
        role = Column(String, nullable=False)
        cree_le = Column(String, nullable=False)
        questionaires_taches=relationship('Questionaire_taches',back_populates='utilisateur',cascade='all, delete')
        questionaires_connaissances=relationship('Questionaire_connaissances',back_populates='utilisateur',cascade='all, delete')
        questionaires_resultats=relationship('Questionaire_resultats',back_populates='utilisateur',cascade='all, delete')


  


class Questionaire_taches(base):
    
        __tablename__ = 'questionaire_taches'

        qstt_id = Column(Integer, primary_key=True, autoincrement=True)
    
        lang = Column(String)

        freq = Column(Boolean, nullable = False)
        nuage = Column(Boolean, nullable = False)
        resum = Column(Boolean, nullable = False)
        cree_le = Column(String)

        utilisateur_id = Column(Integer, ForeignKey("utilisateur.utilisateur_id"))
        utilisateur = relationship("Utilisateur", back_populates="questionaires_taches")
        fichiers_resulats = relationship('Fichier_resulat',back_populates='questionaire_taches',cascade='all, delete')
        # fichiers_entrants = relationship('Fichier_entrant',back_populates='questionaire_taches',cascade='all, delete')

class Questionaire_connaissances(base):

        __tablename__ = "questionaire_connaissances"

        qstc_id = Column(Integer, primary_key=True, autoincrement=True)
        lang = Column(String)
        suj_imp = Column(Boolean)
        doc_vol = Column(Boolean)
        cree_le = Column(String)

        utilisateur_id = Column(Integer, ForeignKey("utilisateur.utilisateur_id"))
        utilisateur = relationship("Utilisateur", back_populates="questionaires_connaissances")
        fichiers_resulats = relationship('Fichier_resulat',back_populates='questionaire_connaissances',cascade='all, delete')
        # fichiers_entrants = relationship('Fichier_entrant',back_populates='questionaire_connaissances',cascade='all, delete')

class Questionaire_resultats(base):

        __tablename__ = "questionaire_resultats"

        qstr_id = Column(Integer, primary_key=True, autoincrement=True)
        lang = Column(String)
        tbl = Column(Boolean)
        img = Column(Boolean)
        txt = Column(Boolean)
        cree_le = Column(String)

        utilisateur_id = Column(Integer, ForeignKey("utilisateur.utilisateur_id"))
        utilisateur = relationship("Utilisateur", back_populates="questionaires_resultats")
        fichiers_resulats = relationship('Fichier_resulat',back_populates='questionaire_resultats',cascade='all, delete')
        # fichiers_entrants = relationship('Fichier_entrant',back_populates='questionaire_resultats',cascade='all, delete')


class Fichier_resulat(base):
        __tablename__ = "fichier_resulats"

        fr_id = Column(Integer, primary_key=True, autoincrement=True)

        nomf = Column(String)
        type_f = Column(String)
        chemin_f = Column(String)
        cree_le = Column(String)

        qstc_id = Column(Integer, ForeignKey("questionaire_connaissances.qstc_id"))
        questionaire_connaissances = relationship("Questionaire_connaissances", back_populates="fichiers_resulats")

        qstt_id = Column(Integer, ForeignKey("questionaire_taches.qstt_id"))
        questionaire_taches = relationship("Questionaire_taches", back_populates="fichiers_resulats")

        qstr_id = Column(Integer, ForeignKey("questionaire_resultats.qstr_id"))
        questionaire_resultats = relationship("Questionaire_resultats", back_populates="fichiers_resulats")

# class Fichier_entrant(base) :

#         __tablename__ = "fichier_entrant"

#         fe_id = Column(Integer, primary_key=True, autoincrement=True)
#         nomf = Column(String)
#         type_f = Column(String)
#         importe_le = Column(String)

#         qstc_id = Column(Integer, ForeignKey("questionaire_connaissances.qstc_id"))
#         questionaire_connaissances = relationship("Questionaire_connaissances", back_populates="fichiers_entrants")

#         qstt_id = Column(Integer, ForeignKey("questionaire_taches.qstt_id"))
#         questionaire_taches = relationship("Questionaire_taches", back_populates="fichiers_entrants")

#         qstr_id = Column(Integer, ForeignKey("questionaire_resultats.qstr_id"))
#         questionaire_resultats = relationship("Questionaire_resultats", back_populates="fichiers_entrants")



try:
        engine = create_engine(f'sqlite:///db1.db')
        
        tb_us_existe = inspect(engine).has_table("Utilisateur")
        from sqlalchemy.orm import sessionmaker
    
        Session = sessionmaker(bind=engine, autoflush=False)
        session = Session()

        if not tb_us_existe:
            base.metadata.create_all(engine)

except:
        print("Impossible de se connecter a la la base de donner.")

        



def main():

    """TM/KM Aquisition"""
    
    st.title("TM/KM Aquisition system")

    if os.path.isfile("t.owl"):
        onto  = get_ontology("t.owl").load()
    else :
        
        onto  = get_ontology("http://test.org/t.owl")

        with onto :

                #ontologie du domaine
                class Network(Thing):
                    pass
                class TraficEntity(Network):
                    pass
                
                class Datagram(TraficEntity):
                    pass
                
                class Flow(TraficEntity):
                    pass

                #

                class Event(Network):
                    pass
                
                class Trafic_Evt(Event):
                    pass
                ##
                class LinkUsageEvt(Trafic_Evt):
                    pass
                class QueueUsage_Evt(Trafic_Evt):
                    pass
                class QueueOverflow_Evt(QueueUsage_Evt):
                    pass

                
                class LostPkt_Evt(Event):
                    pass
                ##
                class  TTL_LostPkt(LostPkt_Evt):
                    pass
                class RT_LostPkt(LostPkt_Evt):
                    pass

                
                class StateChange_Evt(Event):
                    pass
                ##
                class NodeStateChange(StateChange_Evt):
                    pass
                class LinkStateChange(StateChange_Evt):
                    pass
                class RTChange(StateChange_Evt):
                    pass
                class IFaceStateChange(StateChange_Evt):
                    pass
                
                # 
                class Database(Network):
                    pass
                ##
                class LDB(Database):
                    pass
                class GDB(Database):
                    pass


                #
                class ManagementTool(Network):
                    pass

                class Capsule(ManagementTool):
                    pass
                
                class Sensor(ManagementTool):
                    pass
                ##
                class NeighStatus_Sns(Sensor):
                    pass
                class RoutingTable_Sns(Sensor):
                    pass
                class RT_LostPkt_Sns(Sensor):
                    pass
                class TTL_LostPktSns(Sensor):
                    pass
                class QueueUsage_Sns(Sensor):
                    pass
                class LinkUsage_Sns(Sensor):
                    pass

                #
                class TraficStatistics(Network):
                    pass
                ##
                class TMEntry(TraficStatistics):
                    pass
                class TraficMatrix(TraficStatistics):
                    pass
                
                #
                class Action(Network):
                    pass
                ##
                class RestoreAction(Action):
                    pass
                ###
                class RestoreIFace_Actn(RestoreAction):
                    pass
                class RestoreLink_Actn(RestoreAction):
                    pass
                class RestoreNode_Actn(RestoreAction):
                    pass
                class CommandAction(Action):
                    pass
                ####
                class SwitchLink_Cmd(CommandAction):
                    pass
                class ChangeLinkCoast_Cmd(CommandAction):
                    pass
                class TuneSensor_Cmd(CommandAction):
                    pass
                class SwitchRecording_Cmd(TuneSensor_Cmd):
                    pass
                class ChangeSamlingRate_Cmd(TuneSensor_Cmd):
                    pass
                class ChangeRecordingRate_Cmd(TuneSensor_Cmd):
                    pass
                class ChangethresholdRate_Cmd(TuneSensor_Cmd):
                    pass
                class ChangeSwitchNotification_Cmd(TuneSensor_Cmd):
                    pass
                class DBAction(Action):
                    pass
                class BasicAction(Action):
                    pass
                class NotifyAction(Action):
                    pass

                #
                class Demand(Network):
                    pass
                ##
                class DMEntry(Demand):
                    pass
                class DemandMatrix(Demand):
                    pass




                #
                class Actor(Network):
                    pass
                
                class PLA(Actor):
                    pass

                class Reasoner(Actor):
                    pass
                ##
                class OffLR(Reasoner):
                    pass
                class OnLR(Reasoner):
                    pass


                #
                class Routing(Network):
                    pass

                #
                class Abnomality(Network):
                    pass

                class RT_Corruption(Abnomality):
                    pass

                class Congestion(Abnomality):
                    pass
                
                class NetDisconnection(Abnomality):
                    pass
                
                class Loop(Abnomality):
                    pass
                
                #
                class NetEntity(Network):
                    pass

                class SWNetEntity(NetEntity):
                    pass

                class HWNetEntity(NetEntity):
                    pass


                ##
                #
                class Node(HWNetEntity):
                    pass
                
                class Host(Node):
                    pass

                class Router(Node):
                    pass

                ##
                class Link(HWNetEntity):
                    pass

                ##
                class IFace(HWNetEntity):
                    pass



                ##
                
                class Queue(SWNetEntity):
                    pass

                class RoutingTable(SWNetEntity):
                    pass
                
                class RTEntry(SWNetEntity):
                    pass






                #ontologie de l'application

                class Document(Thing):
                    pass
                
                class resume(DataProperty): 
                    domain = [Document]
                    range = [str]
                
                class nuage_de_mots(DataProperty):
                    domain = [Document]
                    range = [str]

                class Title(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Creator(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Subject(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Description(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Publisher(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Contributor(DataProperty): 
                    domain = [Document]
                    range = [str]
                
                class Date(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Type(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Format(DataProperty): 
                    domain = [Document]
                    range = [str]
                
                class Identifier(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Source(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Language(DataProperty): 
                    domain = [Document]
                    range = [str]

                class Relation(DataProperty): 
                    domain = [Document]
                    range = [str]
                
                class Coverage(DataProperty): 
                    domain = [Document]
                    range = [str]
                
                class Rights(DataProperty): 
                    domain = [Document]
                    range = [str]
                

                



                
                
                class Term(Thing):
                    pass
                
                class Frequency(Thing):
                    pass
                


                class Cluster(Thing):
                    pass
                class Clustering(Thing):
                    pass
                
                class date(DataProperty):
                    domain = [Frequency, Clustering]
                    range = [str]

                
                class is_in_cluster(ObjectProperty):
                    domain = [Document]
                    range = [Cluster]

                class is_in_clustering(ObjectProperty):
                    domain = [Cluster]
                    range = [Clustering]


                class has_frequency_of(ObjectProperty):
                    domain = [Document]
                    range = [Frequency]

                class his_frequency_is(ObjectProperty):
                        domain = [Term]
                        range = [Frequency]

                class frequency_is(DataProperty):
                    domain = [Frequency]
                    range = [int]
        onto.save(file = "t.owl", format = "rdfxml")
        onto  = get_ontology("t.owl").load()
    
    if not os.path.isfile("terms.csv"):
        dterm  = {"term":[]}
        import pandas as pd 
        d = pd.DataFrame(dterm)
        d.to_csv("terms.csv", index = False)
              




    menu = ["Acceuil","Se Connecter","S'inscrire"]

    choice = st.sidebar.selectbox("Menu",menu)

    if choice == "S'inscrire":
        usern_list = []
        email_list = []
        for us in session.query(Utilisateur).all():
            usern_list.append(us.user_name)
            email_list.append(us.email)

        st.subheader("Creer un nouveau compte")
        with st.form("my_form"):
            nom = st.text_input("Nom")
            prenom = st.text_input("Prenom")
            
            usrn = st.text_input("Nom d'utilisateur")
            email = st.text_input("Email")
            nv_mdp = st.text_input("Mot de passe",type='password')
            
            date_naissance = st.date_input("Date de naissance", value = datetime.date(1990, 1, 1), min_value=datetime.date(1900,1,1))
            nom_service = st.text_input("Nom de service")
            role = st.selectbox("Role",["Ingenieur de Connaissance", "Autre Employé"])
            sbutton = st.form_submit_button("S'inscrire")
        regex = re.compile(r'([A-Za-z0-9]+[.-_])*[A-Za-z0-9]+@[A-Za-z0-9-]+(\.[A-Z|a-z]{2,})+')
        
        if sbutton:
            if nom == "" or  prenom == "" or  usrn == "" or  email=="" or nv_mdp=="" or nom_service == "":
                 st.error("Les champs ne doivent pas etre vide")
            
            elif usrn == "admin" :
                st.error("Vous ne pouver s'inscrire sous le nom d'utiliasteur : admin")
            elif usrn in usern_list:
                st.error("Nom d'utilisateur deja utilisé, veuillez essayer un autre")
            elif  not usrn.isalnum :
                st.error("Le Nom d'utilisateur doit etre Alphanumerique")            
            elif not nom.isalpha :
                st.error("Le nom doit etre en carcteres alphabetiques")
            elif not prenom.isalpha:
                st.error("Le prenom doit etre en caractere alphabetiaques")
            elif not  re.fullmatch(regex, email):
                st.error("L'email doit etre sous la forme de cette exemple : email@doamin.com ")
            elif email in email_list:
                st.error("Cet email a deja un compte.")
            elif len(nv_mdp) < 8 :
                st.error("Le mot de passe doit contenir au moins 8 caractere")

            else :

                today = datetime.date.today()
                cree_le = str(today)
                h_mdp = hacher(nv_mdp)
                u_ins = Utilisateur(nom = nom, prenom = prenom, user_name = usrn,
                email =  email,mdp =  h_mdp,
                date_naissance = date_naissance, 
                nom_service =  nom_service,
                role = role, cree_le =  cree_le)
                session.add(u_ins)
                session.commit()
                st.success("Compte creé avec succés")
                st.info("Allez vers le menu se connecter")

    elif choice == "Se Connecter":

        st.subheader("Connexion")

        usernamec = st.sidebar.text_input("Nom d'utilisateur")
        password = st.sidebar.text_input("Mot de passe",type='password')

        if st.sidebar.checkbox("Se Connecter"):

            # result = verif_conx(password,usernamec)
            result=False
            results = session.query(Utilisateur).all()
 
            for r in results:

                if (r.user_name == usernamec) and (hacher(password) == r.mdp):
                    print(r.mdp)
                    result = True
            
            
            if result:
                st.success("Connecté en tant que : {}".format(usernamec))
                

                menu_c = ["Aquisition de Connaissance","Visualisaton","Formalisation","Profile"]

                choice_c = st.selectbox("Menu",menu_c) 

                if choice_c == "Aquisition de Connaissance":             
                    

                    st.subheader("Changer de Langue")
                    
                    langs = ["Anglais","Français"]
                    global lang
                    lang = st.selectbox("Langues",langs)

                    st.subheader("Charger les documents")

                    

                    uploaded_files = st.file_uploader("Choisir les documents",
                    accept_multiple_files=True)

                    nb_doc = len(uploaded_files)

                        
                    st.text("Nombre de Documents : " + str(nb_doc))

                    if nb_doc>0:
                        st.subheader("Besoins")

                        besoins = ["Selon Connaissance","Selon Taches","Selon Formats de Resultat", "Charger les Resultats des outils externes"]

                        besoin = st.selectbox("Besoins",besoins)

                        fdatas_list = []
                        for f in uploaded_files:

                            string_data = retouner_string(f)
                            filename = Path(f.name)

                            filename = filename.with_suffix('')
                            fdata = (string_data, str(filename))
                            fdatas_list.append(fdata)

                        if besoin == "Selon Taches":

                            st.subheader("Selon Taches : ")
                            with st.form(key = "tach_form"):
                                term_freq = st.checkbox("Frequence de terms")
                               

                                nuage_mots = st.checkbox('Nuage de mots')
                                
                                    
                                resume = st.checkbox("Resumé")

                                clust = st.checkbox("Clustering")

                                clas = st.checkbox("Classification")
                                

                                tache_btn = st.form_submit_button("Executer les taches")
                            



                            if tache_btn:

                                st.subheader("Appercu des resultats:")
                                
                                if term_freq :
                                   term_freq_ui(fdatas_list)

                                if nuage_mots:

                                    nuage_mots_ui(fdatas_list)                                    

                                if  resume :

                                    resume_ui(fdatas_list)

                                if clust or clas :
                                    st.subheader("Clustering et Classification")
                                    st.info("Charger les resultas depuis Orange 3 dans la section charger les resultats des outils externes du menu besoins ")
                                    os.system("python3 -m Orange.canvas")



                                if not term_freq and not nuage_mots and not resume and not clust and  not clas:
                                    nuage_mots(fdatas_list)

                        if besoin == "Selon Connaissance" :
                            st.subheader("Selon Connaissance : ")
                            with st.form(key = "connais_form"):
                                rp_freq_ng = st.radio(
                                "Voulez vous connaitre les sujets les plus important que contients les documents",
                                ('Oui', 'Non'))  
                                
                                    
                                rp_resum = st.radio(
                                "Pensez vous que les documents son volumineux",
                                ('Oui', 'Non'))


                                rp_clust = st.radio(
                                "Voulez vous grouper les documents selon leur ressemblance",
                                ('Oui', 'Non'))

                                rp_clas = st.radio(
                                "Les documents sont ils classés selon des themes ou  selon un autre critere ",
                                ('Oui', 'Non'))


                                

                                connais_btn = st.form_submit_button("Traiter les reponses")
                            

                            if   connais_btn :
                                st.subheader("Appercu des resultats:")
                                if rp_freq_ng == "Oui":
                                
                                    term_freq_ui(fdatas_list)
                                    nuage_mots_ui(fdatas_list)
                                    
                                if rp_resum == "Oui":
                                    resume_ui(fdatas_list)
                                if rp_clust =="Oui" or rp_clas == "Oui" :
                                    st.subheader("Clustering et Classification")
                                    st.info("Charger les resultas depuis Orange 3 dans la section charger les resultats des outils externes du menu besoins ")

                                    os.system("orange-canvas")

                                if rp_freq_ng == "Non" and rp_resum == "Non" and rp_clust == "Non" and rp_clas == "Non" :
                                    nuage_mots_ui(fdatas_list)
                        if besoin == "Selon Formats de Resultat":
                            st.subheader("Selon Formats de Resultat : ")

                            with st.form(key = "frmt_results"):
                                rp_tbl = st.checkbox("Tableaux CSV")
                                
                                rp_img  = st.checkbox("Image")
                   
                                rp_txt = st.checkbox("Fichier Text")
                                


                                sub_frmt_result = st.form_submit_button("Extraire ces resulats") 

                            if sub_frmt_result :
                            
                                st.subheader("Appercu des resultats:")


                                if rp_tbl :
                                     
                                    term_freq_ui(fdatas_list)
                                    st.subheader("Clustering et Classification")
                                    st.info("Charger le Resultas depuis Orange 3 dans la section Charger Resultats externes du menu besoins ")
                                    os.system("orange-canvas")

                                if rp_img :
                                    
                                    nuage_mots_ui(fdatas_list)

                                if rp_txt : 

                                    resume_ui(fdatas_list)
                                
                                # if rp_clust or rp_class :
                                    

                                if not rp_tbl and not rp_img and not rp_txt and not rp_clust and not rp_class :

                                    nuage_mots_ui(fdatas_list)  

                        if besoin  == "Charger les Resultats des outils externes":
                            
                            from pandas import read_csv

                            st.subheader("Charger les resultas  de clustering")
                            with st.form("upload_files_clust"):

                                uploaded_files_clust = st.file_uploader("Selectioner les Resultats de Clustering (Depuis Orange 3)",
                                accept_multiple_files=True)

                                sb_up_clst = st.form_submit_button("Charger")

                            nb_doc_clust = len(uploaded_files_clust)
                            st.text("Nombre de resultats chargés : " + str(nb_doc_clust))

                            if sb_up_clst:

                                for f_clust in uploaded_files_clust:
                                    cf  = read_csv(f_clust)
                                    cf.to_csv(CLUSTER_DIR+"/"+str(datetime.date.today())+"_"+f_clust.name, index=False)
                                st.success("Les resultats sont chargés dans la base de resultats avec succés", icon="✅")
                                
                                with onto : 
                                    from onto_class import Clustering, Cluster, Document
                                    clustering_num = str(secrets.token_hex(nbytes=8))
                                    clustering = Clustering(clustering_num) 
                                    
                                    today = datetime.date.today()
                                    clustering.date.append(today)
                                    
                                    
                                    for i in cf.index:
                                        if i>1:
                                                
                                                doc = Document(cf["name"][i])
                                                cluster = Cluster(str(cf["Cluster"][i])+"_"+clustering_num)
                                                doc.is_in_cluster.append(cluster)
                                                Clustering.is_in_clustering.append(clustering)
                                    onto.save(file = "t.owl", format = "rdfxml")
                                st.subheader("Charger les resultas  de classification")

                            with st.form("upload_files_class"):
                                uploaded_files_clas = st.file_uploader("Selectioner les Resultats de Classification (Depuis Orange 3)",
                                accept_multiple_files=True)

                                sb_up_clas = st.form_submit_button("Charger")
                            nb_doc_clas = len(uploaded_files_clas)
                            st.text("Nombre de resultats chargés : " + str(nb_doc_clas))

                            if sb_up_clas:
                                for f_clas in uploaded_files_clas:
                                    cf  = read_csv(f_clas)
                                    cf.to_csv(CLASSIF_DIR+"/"+str(datetime.date.today())+"_"+f_clas.name, index=False)
                                st.success("Les resultats sont chargés dans la base de resultats avec succés", icon="✅")

                                


                        


                if choice_c == "Visualisaton" :
                    

                    if usernamec == "admin":
                        st.subheader("Les dossiers contenants")

                       
                        col1, col2, col3 = st.columns(3)

                        with col1:
                            if st.button("Frequences de termes"):
                                ouvrir_dossier(FREQ_TERM_DIR)

                        with col2:
                            if st.button("Nuages de mots"):
                                ouvrir_dossier(NUAGES_MOTS_DIR)

                        with col3:
                            if st.button("Resumés"):
                                ouvrir_dossier(RESUMES_DIR)

                    
                    st.subheader("Base des resultas :")

                    resultats = ["Freqence des termes","Nuage de mots","Resumes","Clustering","Classification"]

                    resultat = st.selectbox("Type de resultats",resultats)                   

                    if resultat == "Freqence des termes" :
                        import pandas as pd
                        fileslist =  os.listdir(FREQ_TERM_DIR)
                        with st.form(key="searchform"):
                            
                            
                            rech_result = st.text_input("Recherche")
                            
                            submit_search = st.form_submit_button(label="Rechercher")
                        print("vide: "+str(rech_result))
                     
                        cont_disp = st.empty()
                        cont_bool=True
                        @st.cache
                        def convert_df(df):
                            # IMPORTANT: Cache the conversion to prevent computation on every rerun
                            return df.to_csv().encode('utf-8')

                       
                        with cont_disp.container():
                                for file in fileslist:
                                    df = pd.read_csv(FREQ_TERM_DIR+"/"+file)
                            
                                    with st.expander("Frequence des termes : "+ file):
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        st.dataframe(df)

                            
                        if submit_search:
                            if cont_bool == True:
                                cont_disp.empty()
                                cont_bool = False
                            
                            if rech_result == "":
                                for file in fileslist:
                                    df = pd.read_csv(FREQ_TERM_DIR+"/"+file)

                                    with st.expander("Frequence des termes : "+ file):
                                          
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        st.dataframe(df)

                            else :
                                for file in fileslist :
                                    if file.lower().find(rech_result.lower()) != -1:
                                        df = pd.read_csv(FREQ_TERM_DIR+"/"+file)
                                        with st.expander("Frequence des termes : "+ file):
                                            
                                            csv = convert_df(df)

                                            st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                            )
                                            st.dataframe(df)




                    if resultat == "Nuage de mots" :
                        
                        from PIL import Image
                        fileslist =  os.listdir(NUAGES_MOTS_DIR)
                        with st.form(key="searchform"):
                            
                            
                            rech_result = st.text_input("Recherche")
                            
                            submit_search = st.form_submit_button(label="Rechercher")                     
                        cont_disp = st.empty()
                        cont_bool = True
                        with cont_disp.container() :
                            
                            for file in fileslist:
                                image = Image.open(NUAGES_MOTS_DIR+"/"+file)
                                with st.expander("Nuage de mot de : "+ file):
                                    
                                    
                                    with open(NUAGES_MOTS_DIR+"/"+file, "rb") as dfile:
                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger l'image",
                                            data=dfile,
                                            file_name=file,
                                            mime="image/png"
                                            )
                                    st.image(image, caption=file)

                                image.close()
                                dfile.close()
                            
                        if submit_search:
                            if cont_bool == True:
                                cont_disp.empty()
                                cont_bool=False

                            if rech_result == "":
                                    
                                    for file in fileslist:
                                            
                                        image = Image.open(NUAGES_MOTS_DIR+"/"+file)
                                        with st.expander("Nuage de mot de : "+ file):
                                            with open(NUAGES_MOTS_DIR+"/"+file, "rb") as dfile:
                                                st.download_button(
                                                            key=str(secrets.token_hex(nbytes=8)),
                                                            label="Telechager l'image",
                                                            data=dfile,
                                                            file_name=file,
                                                            mime="image/png"
                                                    )
                                            st.image(image, caption=file)
                                        image.close()
                                        dfile.close()
                                                                                       
                            else :

                                    for file in fileslist :
                                        if file.lower().find(rech_result.lower()) != -1:
                                            image = Image.open(NUAGES_MOTS_DIR+"/"+file)
                                            with st.expander("Nuage de mot de : "+ file):
                                                with open(NUAGES_MOTS_DIR+"/"+file, "rb") as dfile:
                                                    st.download_button(
                                                            key=str(secrets.token_hex(nbytes=8)),
                                                            label="Telecharger l'image",
                                                            data=dfile,
                                                            file_name=file,
                                                            mime="image/png"
                                                    )
                                                st.image(image, caption=file)
                                            image.close()
                                            dfile.close()




                    if resultat == "Resumes" :

                        fileslist =  os.listdir(RESUMES_DIR)
                        with st.form(key="searchform"):
                            
                            
                            rech_result = st.text_input("Recherche")
                            
                            submit_search = st.form_submit_button(label="Rechercher")                     
                        cont_disp = st.empty()
                        cont_bool = True
                        with cont_disp.container():
                                for file in fileslist:
                                    
                                    bfile = open(RESUMES_DIR+"/"+file, "r")
                                    text_of_file = bfile.read()
                                    with st.expander("Resumé : "+ file):
                                        st.download_button(key=str(secrets.token_hex(nbytes=8)), 
                                        label = 'Telecharger le text', 
                                        data = text_of_file, 
                                        file_name=file)

                                        st.text(text_of_file)  

                                    bfile.close()    
                        if submit_search:
                            if cont_bool == True:
                                cont_disp.empty()
                                cont_bool = False

                            if rech_result == "":
                                
                                for file in fileslist:
                                    bfile = open(RESUMES_DIR+"/"+file, "r")
                                    text_of_file = bfile.read()
                                    with st.expander("Resumé : "+ file):
                                        st.download_button(key=str(secrets.token_hex(nbytes=8)), 
                                        label = 'Telecharger le text', 
                                        data = text_of_file, 
                                        file_name=file)

                                        st.text(text_of_file)
                                    bfile.close()
  
                            else :
                                for file in fileslist :
                                    if file.lower().find(rech_result.lower()) != -1:
                                        bfile = open(RESUMES_DIR+"/"+file, "r")
                                        text_of_file = bfile.read()
                                        with st.expander("Resumé : "+ file):
                                            st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)), 
                                            label = 'Telecharger le text', 
                                            data = text_of_file, 
                                            file_name=file)

                                            st.text(text_of_file)
                                            
                                        bfile.close()


                    if resultat == "Clustering" :
                        import pandas as pd
                        fileslist =  os.listdir(CLUSTER_DIR)
                        with st.form(key="searchform"):
                            
                            
                            rech_result = st.text_input("Recherche")
                            
                            submit_search = st.form_submit_button(label="Rechercher")
                        print("vide: "+str(rech_result))
                     
                        cont_disp = st.empty()
                        cont_bool=True
                        @st.cache
                        def convert_df(df):
                            # IMPORTANT: Cache the conversion to prevent computation on every rerun
                            return df.to_csv().encode('utf-8')

                       
                        with cont_disp.container():
                                for file in fileslist:
                                    df = pd.read_csv(CLUSTER_DIR+"/"+file)
                                    
                                    with st.expander("Clustering : "+ file):
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        df=df.drop(df.index[[0,1]])
                                        df = df.reset_index()
                                        df = df.drop(["index"], axis=1)
                                        
                                        st.dataframe(df)

                        if submit_search:
                            if cont_bool == True:
                                cont_disp.empty()
                                cont_bool = False
                            
                            if rech_result == "":
                                for file in fileslist:
                                    df = pd.read_csv(CLUSTER_DIR+"/"+file)

                                    with st.expander("Clustering : "+ file):
                                          
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        st.dataframe(df)

                            else :
                                for file in fileslist :
                                    if file.lower().find(rech_result.lower()) != -1:
                                        df = pd.read_csv(CLUSTER_DIR+"/"+file)
                                        with st.expander("Clustering : "+ file):
                                            
                                            csv = convert_df(df)

                                            st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                            )
                                            st.dataframe(df)


                    if resultat == "Classification" :
                        import pandas as pd
                        fileslist =  os.listdir(CLASSIF_DIR)
                        with st.form(key="searchform"):
                            
                            
                            rech_result = st.text_input("Recherche")
                            
                            submit_search = st.form_submit_button(label="Rechercher")
                        print("vide: "+str(rech_result))
                     
                        cont_disp = st.empty()
                        cont_bool=True
                        @st.cache
                        def convert_df(df):
                            # IMPORTANT: Cache the conversion to prevent computation on every rerun
                            return df.to_csv().encode('utf-8')

                       
                        with cont_disp.container():
                                for file in fileslist:
                                    df = pd.read_csv(CLASSIF_DIR+"/"+file)
                            
                                    with st.expander("Classification : "+ file):
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        st.dataframe(df)


                            
                        if submit_search:
                            if cont_bool == True:
                                cont_disp.empty()
                                cont_bool = False
                            
                            if rech_result == "":
                                for file in fileslist:
                                    df = pd.read_csv(CLASSIF_DIR+"/"+file)

                                    with st.expander("Classification : "+ file):
                                          
                                        csv = convert_df(df)

                                        st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                        )
                                        st.dataframe(df)

                            else :
                                for file in fileslist :
                                    if file.lower().find(rech_result.lower()) != -1:
                                        df = pd.read_csv(CLASSIF_DIR+"/"+file)
                                        with st.expander("Frequence des termes : "+ file):
                                            
                                            csv = convert_df(df)

                                            st.download_button(
                                            key=str(secrets.token_hex(nbytes=8)),
                                            label="Telecharger en tant que CSV",
                                            data=csv,
                                            file_name=file,
                                            mime='text/csv',
                                            )
                                            st.dataframe(df)


                if choice_c == "Formalisation":
                    
                    st.subheader("Formalisation")
                    
                    tsks = ["Attribution des termes au concepts","Telecharger l'ontologie"]

                    
                    menu_f = st.selectbox("Taches", tsks)                   
                    
                    if menu_f == "Attribution des termes au concepts":
                        st.subheader("Attribution des termes au concepts de l'ontologies")
                        with st.form("my_form"):
                            col1f, col2f = st.columns(2)
                            
                            with col1f:
                                #st.header("A cat")
                                import pandas as pd
                                tro = pd.read_csv("terms.csv")
                                tr = pd.read_csv("terms.csv")
                                for i in tr.index:
                                    tr["term"][i] = str(tr["term"][i]).split(".")[1]
                                trs = tr["term"].to_list()
                                terc = st.selectbox("Terme",trs)             
                            with col2f:
                                #st.header("A dog")
                                onto = get_ontology("t.owl").load()
                                from onto_class import Network,Term
                                with onto:
                                    cts = list(Network.descendants())
                                    for i in range(len(cts)):
                                        print(cts[i])
                                        cts[i]=str(cts[i]).split(".")[1]
                                    #cts = cts.split(".")[1]
                                    cnpc = st.selectbox("Concept",cts)  
                            
                            submittedF= st.form_submit_button("Attribuer")
                            if submittedF and cnpc and terc:

                                        print(str(terc))
                                        termF = Term(str(terc))
                                        print(termF)
                                        
                                        import onto_class
                                        module = onto_class
                                        class_ = getattr(module, str(cnpc))
                                        print(class_)
                                        termF.is_a.append(class_)
                                        onto.save(file = "t.owl", format = "rdfxml")
                                        for i in tr.index:
                                            if tro["term"][i] == "t."+terc :      
                                                tro = tro.drop(i)    
                                        tro.to_csv('terms.csv', index = False)
                                        st.success('Le Terme '+str(terc)+" a été attribué au concept "+str(cnpc)+" avec succés" , icon="✅")
                                        import time
                                        time.sleep(2)
                                        st.experimental_rerun()
                    if menu_f == "Telecharger l'ontologie":

                        st.subheader("Telecharger l'ontologie")
                        col1dw, col2dw, col3dw = st.columns(3)

                        with col1dw:
                            pass
                        with col2dw:
                            with open("t.owl", "rb") as filedowl:
                                btn = st.download_button(
                                label="Telecharger l'ontologie",
                                data=filedowl,
                                file_name="t.owl",
                                mime="text/owl"
                                )
                        with col3dw:
                            pass



                if choice_c == "Profile":
                    usern_list = []
                    email_list = []
                    for us in session.query(Utilisateur).all():
                        usern_list.append(us.user_name)
                        email_list.append(us.email)
                    st.subheader("Profile")
                    from pandas import DataFrame
                    user = session.query(Utilisateur).filter(Utilisateur.user_name == usernamec).first()
                    
                    st.subheader("Modifier le compte "+ user.user_name)

                    nom = st.text_input(label="Nom" , key=str(secrets.token_hex(nbytes=8)))
                    nom_btn = st.button(label="Modifier le nom", key=str(secrets.token_hex(nbytes=8)))
                    if nom_btn:
                        user.nom=nom
                        session.commit()
                       
                    prenom = st.text_input(label="Prenom", key=str(secrets.token_hex(nbytes=8)))
                    if st.button(label="Modifier le prenom", key=str(secrets.token_hex(nbytes=8))):
                        user.prenom=prenom
                        session.commit()
                    usernom = st.text_input(label="Nom d'utilisateur", key=str(secrets.token_hex(nbytes=8)))
                    if st.button(label="Modifier le nom d'utilisateur", key=str(secrets.token_hex(nbytes=8))):
                        if usernom == "admin" :
                            st.error("Vous ne pouver utiliser le nom d'utiliasteur : admin")
                        elif user.user_name == "admin":
                            st.error("l'admin ne peut pas chager son nom d'utilisateur")
                        elif usernom in usern_list:
                            st.error("Nom d'utilisateur deja utilisé, veuillez essayer un autre")
                        elif  not usernom.isalnum :
                            st.error("Le Nom d'utilisateur doit etre Alphanumerique")                         
                        else:
                            user.user_name=usernom
                            session.commit()

                    email = st.text_input(label="Email", key=str(secrets.token_hex(nbytes=8)))
                    if st.button(label="Modifier l'email", key=str(secrets.token_hex(nbytes=8))):
                        if not  re.fullmatch(regex, email):
                            st.error("L'email doit etre sous la forme de cette exemple : email@doamin.com ")
                        elif email in email_list:
                            st.error("Cet email a deja un compte.")
                        else:
                            user.email=email
                            session.commit()
                    anc_mdp = st.text_input("Ancien mot de passe", type='password', key=str(secrets.token_hex(nbytes=8)))
                    nv_mdp = st.text_input("Nouveau mot de passe", type='password', key=str(secrets.token_hex(nbytes=8)))
                    if st.button(label="Modifier le mot de passe", key=str(secrets.token_hex(nbytes=8))):
                        if hacher(anc_mdp) == user.mdp:
                            user.mdp=nv_mdp
                        else:
                            st.error("Ancien mot de passe incorrect")
                    date_naissance = st.date_input(label="Date de naissance", key=str(secrets.token_hex(nbytes=8)), value = datetime.date(1990, 1, 1), min_value=datetime.date(1900,1,1))
                    if st.button(label="Modifier la date de naissance", key=str(secrets.token_hex(nbytes=8))):
                        user.date_naissance=str(date_naissance)
                    nom_service = st.text_input(label="Nom de service", key=str(secrets.token_hex(nbytes=8)))
                    if st.button(label="Modifier le Nom de service", key=str(secrets.token_hex(nbytes=8))):
                        user.nom_service = nom_service
                    role = st.selectbox("Role",["Ingenieur de Connaissance", "Autre Employé"])
                    if st.button(label="Modifier le Role", key=str(secrets.token_hex(nbytes=8))):
                        user.role=role
                    user = session.query(Utilisateur).filter(Utilisateur.user_name == usernamec).first()
                        
                    cmpt = DataFrame([user.nom, user.prenom, user.email, user.date_naissance, user.nom_service, user.role],
                    ("Nom","Prenom","Email","Date de naissance", "Nom de service", "Role"))
                    st.subheader("Information du compte")
                    st.dataframe(cmpt, width= 1000)

                        # session.query(Utilisateur.user_name = ).
                        # filter(Utilisateur.username == usernamec).\
                        # update({'nom': nom, 'prenom'=prenom, email  })
                        # session.commit()


            else:
                st.warning("Mot de passe/ Nom d'utilisateur incorrect")


    elif choice == "Acceuil":
        
        # st.header("Bienvenue au TM/KM aquisition system")
        st.text("\n")
        from PIL import Image
        image = Image.open('icon.png')

        st.image(image, use_column_width="always")

if __name__ == '__main__':
	main()